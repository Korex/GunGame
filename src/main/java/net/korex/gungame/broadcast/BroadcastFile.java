package net.korex.gungame.broadcast;

import net.korex.gungame.FileManager;
import net.korex.gungame.Main;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BroadcastFile extends FileManager {
    public BroadcastFile() {
        super("broadcast");
        if(this.getMessages() == null || this.getMessages().size() == 0) {
            List<String> list = new ArrayList<>();
            list.add("Du bekommst mit &a/hilfe &7Hilfe!");
            list.add("Erhalte Items mit &a&l/kit&r&7!");
            list.add("Vote mit &a&l/vote &r&7um &a&l75 Coins &r&7zu erhalten!");
            list.add("Events sind am &a&lMittwoch und Samstag um 19:30&r&7!");
            this.getCfg().set("messages", list);
            this.save();
        }
    }

    public List<String> getMessages() {
        List<String> list = this.getCfg().getStringList("messages");
        return list;
    }

    public String getRandomMessage() {
        List<String> list = this.getMessages();
        if(list == null || list.size() == 0){
            return null;
        }
        Random random = new Random();
        int i = random.nextInt(list.size());
        String message = list.get(i);
        message = ChatColor.translateAlternateColorCodes('&', message);
        return Main.PR + " " + message;
    }


}
