package net.korex.gungame.broadcast;

import net.korex.gungame.Main;
import org.bukkit.Bukkit;

public class Broadcast {

    public static void start() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(),() -> {
            BroadcastFile file = new BroadcastFile();
            String message = file.getRandomMessage();
            if(message != null) {
                Bukkit.broadcastMessage(message);
            }
        }, 0, 20*60*2);
    }

}
