package net.korex.gungame.information;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import net.korex.gungame.Main;

public class DeniedCommands implements Listener {

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (!e.getPlayer().hasPermission(Main.ADMIN)) {
			String[] message = e.getMessage().split(" ");

			if (message[0].equalsIgnoreCase("/help") || message[0].equalsIgnoreCase("/pl") || message[0].equalsIgnoreCase("/?")
					|| message[0].equalsIgnoreCase("/bukkit:?") || message[0].equalsIgnoreCase("/information")) {
				MessagesHelpCommands.help(e.getPlayer());
				e.setCancelled(true);
			}
		}
	}

}
