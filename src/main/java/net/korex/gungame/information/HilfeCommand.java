package net.korex.gungame.information;

import net.korex.gungame.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HilfeCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		
		if(sender instanceof Player) {
			Player p = (Player) sender;
			Messages.form(p, "Dir wurde ein §aBuch §7in dein Inventar gelegt, welches Informationen über diesen Server beinhaltet.");
			MessagesHelpCommands.helpBook(p);
		}
		
		return true;
	}

}
