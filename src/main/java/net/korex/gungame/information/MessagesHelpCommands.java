package net.korex.gungame.information;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.korex.gungame.Main;
import net.korex.gungame.Messages;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class MessagesHelpCommands {

    public static void helpBook(Player p) {
        ItemStack item = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta meta = (BookMeta) item.getItemMeta();

        meta.setDisplayName(Main.PR + " Informationen");
        meta.setAuthor("§a_korex");
        meta.setTitle("§3Gun§7-§cGame Informationen");

        meta.setPages("Willkommen auf §cGun§0-§3Game§0!\n\nUnser Discord Server: /discord\n\nUnser Team steht dir bei allen Fragen zur Verfügung!", "§lSpielprinzip:\n§r§0Pro Kill bekommst du ein Level, wenn man stirbt, gehen alle Items verloren. Bei jedem Level bekommst du Level-Items, welche durch die Lore:" + Main.PR + " §rmarkiert sind! Normale Items gehen allerdings nicht bei einem Level-Aufstieg verloren.", "§lCoins:\n\n§rDu bekommst für einen Kill Coins, welche du im Shop am Spawn verwenden kannst.", "§lCommands:\n§l/lobby oder /spawn §r- Teleportiert dich zur LobbyLocation\n§l/stats [<Name>] §r- Zeigt die Stats\n§l/kit §r- Gibt dir ein Kit mit nützlichen Sachen\n§l/anvil §r- Öffnet einen Amboss\n§l/trash §r- Öffnet den Mülleimer\n§l/vote §r - Votekit");

        item.setItemMeta(meta);
        p.getInventory().addItem(item);
    }

    public static void help(Player p) {
        p.sendMessage("§3");
        Messages.form(p, "§aSpielprinzip:");
        p.sendMessage(
                "§3Pro Kill bekommst du ein Level, wenn man stirbt, gehen alle Items verloren. Bei jedem Level bekommst du Level-Items, welche durch die Lore: "
                        + Main.PR + " §3markiert sind. Normale Items gehen allerdings nicht bei einem Level-Aufstieg verloren.");
        p.sendMessage("§3");

        Messages.form(p, "§aCoins");
        p.sendMessage("§3Du bekommst für einen Kill Coins, welche du im Shop am Spawn verwenden kannst.");
        p.sendMessage("§3");

        Messages.form(p, "§aRänge");
        p.sendMessage("§3Ranginfos bekommst du per /rang");
        p.sendMessage("§3");

        Messages.form(p, "§aHilfreiche Commands:");
        p.sendMessage("§3/lobby oder /spawn - Teleportiert dich zur LobbyLocation");
        p.sendMessage("§3/stats [<Name>] - Zeigt die Stats");
        p.sendMessage("§3/kit - Gibt dir ein Kit mit nützlichen Sachen");
        p.sendMessage("§3/anvil - Öffnet einen Amboss");
        p.sendMessage("§3/trash - Öffnet den Mülleimer");
    }

    public static void rangInfos(Player p) {
        Messages.form(p, "§6Premium");
        p.sendMessage("§3- 10 Euro pro Monat");
        p.sendMessage("§3- Premium Kit, alle drei Tage, bestehend aus: 10 E-Perlen, 10 Diamanten, 10 Gapplen, 2 Fluchtseilen, 2 x 64 XP-Flaschen und 16 Büchern");
        p.sendMessage("§3- Größere Netherchest");
        p.sendMessage("§3- vollen Maps joinen");
        p.sendMessage("§3- doppelte Coins");
        p.sendMessage("§3");

        Messages.form(p, "§5YouTuber");
        p.sendMessage("§3- ab 250 Abos + einem mindestens 4 Minuten langen Video von diesem Server");
        p.sendMessage("§3- alle Vorteile des Premium Ranges");
    }

}
