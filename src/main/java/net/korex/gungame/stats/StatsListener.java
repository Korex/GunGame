package net.korex.gungame.stats;

import net.korex.gungame.Messages;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class StatsListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        if (!(e.getEntity().getKiller() instanceof Player)) {
            return;
        }

        Player dead = e.getEntity();
        Player killer = dead.getKiller();

        Stats deadStats = StatsManager.getInstance().getFromCache(dead);
        if (deadStats != null) {
            deadStats.setCoins(deadStats.getCoins() - 1);
            deadStats.addDeath();
        }

        Stats killerStats = StatsManager.getInstance().getFromCache(killer);

        if (killerStats != null) {
            killerStats.addKill();
            if (dead.getLevel() > 0) {
                killerStats.setCoins(killerStats.getCoins() + (dead.getLevel() + 2));
            } else {
                killerStats.setCoins(killerStats.getCoins() + 2);
            }
        }

        double killerHealth = (Math.round(killer.getHealth())) / 2;

        Messages.form(killer, "Du hast §a" + dead.getName() + " §7getötet!");
        Messages.form(killer, "+§a" + (dead.getLevel() + 2) + " §bCoins§7!");

        Messages.form(dead, "Du wurdest von §c" + killer.getName() + " §8[§a" + killerHealth + "§c♥§8] §7getötet!");
        Messages.form(dead, "-§c1§b Coin§7!");

        killer.setLevel(killer.getLevel() + 1);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        StatsManager.getInstance().load(e.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Stats stats = StatsManager.getInstance().getFromCache(e.getPlayer());
        if (stats != null) {
            StatsManager.getInstance().write(stats);
        }
        StatsManager.getInstance().removeFromCache(e.getPlayer());
    }

}
