package net.korex.gungame.stats;

import net.korex.gungame.Messages;
import net.korex.gungame.maps.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatsCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command paramCommand, String paramString, String[] args) {
        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (args.length == 0) {
                p.performCommand("stats " + p.getName());
            } else if (args.length == 1) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    Stats stats = StatsManager.getInstance().getFromCache(target);
                    if(stats == null) {
                        Messages.form(p, "Die Statistiken konnten §cnicht geladen§7 werden!");
                    }
                    Messages.form(p, "§7-------§a " + target.getDisplayName() + " §7-------");
                    Messages.form(p, "§7Coins: §b" +  stats.getCoins());
                    Messages.form(p, "§7Level: §b" +  target.getLevel());
                    Messages.form(p, "§7Kills: §b" +  stats.getKills());
                    Messages.form(p, "§7Deaths: §b" +  stats.getDeaths());

                    if(MapManager.getInstance().inMap(target)) {
                        Messages.form(p,"§7Momentan auf §a" + MapManager.getInstance().getCurrentMap(target).getName() + "§7!");
                    } else {
                        Messages.form(p,"§7Momentan in der §aLobby§7!");
                    }

                } else {
                    Messages.form(p, "Der angegebene Spieler ist §coffline§7!");
                }
            }

        }

        return true;
    }

}
