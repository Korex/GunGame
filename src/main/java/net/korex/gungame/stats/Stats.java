package net.korex.gungame.stats;

import net.korex.gungame.FileManager;
import org.bukkit.entity.Player;

public class Stats {

    private Player player;
    private int kills;
    private int deaths;
    private int coins;

    public Stats(Player player, int kills, int deaths, int coins) {
        this.player = player;
        this.kills = kills;
        this.deaths = deaths;
        this.coins = coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public int getKills() {
        return kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getCoins() {
        return coins;
    }

    public Player getPlayer() {
        return player;
    }

    public void addKill() {
        this.kills = this.kills + 1;
    }

    public void addDeath() {
        this.deaths = this.deaths + 1;
    }
}
