package net.korex.gungame.stats;

import net.korex.gungame.Messages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;

public class StatsEditCommand extends AbstractCommand {

	public StatsEditCommand() {
		super("setcoins");
	}

	@Override
	public void onExecute(Player p, String[] args) {
		System.out.println(args.length);

		if (args.length == 3) {
			if (args[0].equalsIgnoreCase("setcoins")) {
				try {
					String playername = args[1];
					int i = Integer.parseInt(args[2]);

					Player target = Bukkit.getPlayer(playername);

					if (target != null) {
						Stats stats = StatsManager.getInstance().getFromCache(target);
						if(stats == null) {
							Messages.form(p, "Die Statistiken konnten §cnicht geladen§7 werden!");
							return;
						}
						stats.setCoins(i);
						Messages.form(p, "Du hast den Kontostand von §a" + target.getName() + " §7auf §a" + stats.getCoins() + "§7 coins geändert!");
					} else {
						Messages.form(p, "Der angegebene Spieler ist §coffline§7!");
					}
				} catch (NumberFormatException e) {
					sendHelp(p);
				}
			} else {
				sendHelp(p);
			}
		} else {
			sendHelp(p);
		}

	}

	@Override
	public void sendHelp(Player p) {
		p.sendMessage(Main.PR + "§a/gg setCoins <Playername> <Amount>");
	}

}
