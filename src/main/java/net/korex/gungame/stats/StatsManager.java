package net.korex.gungame.stats;

import net.korex.bettersql.SqlColumn;
import net.korex.bettersql.SqlColumnType;
import net.korex.bettersql.SqlResultSet;
import net.korex.bettersql.SqlTable;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.function.Consumer;

public class StatsManager {

    private static StatsManager instance = new StatsManager();

    public static StatsManager getInstance() {
        return instance;
    }

    private SqlTable table;
    private HashMap<Player, Stats> cache;

    private StatsManager() {
        this.table = new SqlTable("GunGame_Stats");
        this.table.create(new SqlColumn("uuid", SqlColumnType.VARCHAR).setLength(64).setPrimary(), new SqlColumn("coins", SqlColumnType.INT), new SqlColumn("kills", SqlColumnType.INT), new SqlColumn("deaths", SqlColumnType.INT));
        this.cache = new HashMap<>();
    }

    public void loadAll() {
        for(Player player : Bukkit.getOnlinePlayers()) {
            this.load(player);
        }
    }

    public Stats getFromCache(Player player) {
        if(cache.containsKey(player)) {
            return cache.get(player);
        } else {
            return null;
        }
    }

    public void removeFromCache(Player player) {
        cache.remove(player);
    }

    public void load(Player player) {
        this.table.get("uuid=" + player.getUniqueId().toString()).async(sqlResultSet -> {
            if (sqlResultSet.size() == 0) {
                Stats stats = new Stats(player, 0, 0, 100);
                this.cache.put(player, stats);
                return;
            }

            int coins = (int) sqlResultSet.get("coins", 0);
            int kills = (int) sqlResultSet.get("kills", 0);
            int deaths = (int) sqlResultSet.get("deaths", 0);

            Stats stats = new Stats(player, kills, deaths, coins);
            this.cache.put(player, stats);
        });
    }

    public void write(Stats stats) {
        this.table.contains("uuid=" + stats.getPlayer().getUniqueId().toString()).async(aBoolean -> {
            if (aBoolean == null || !aBoolean) {
                this.table.insert("uuid=" + stats.getPlayer().getUniqueId().toString() + ";coins=" + stats.getCoins() + ";kills=" + stats.getKills() + ";deaths=" + stats.getDeaths()).sync();
                return;
            }

            this.table.update("coins=" + stats.getCoins() + ";kills=" + stats.getKills() + ";deaths=" + stats.getDeaths(), "uuid=" + stats.getPlayer().getUniqueId().toString()).sync();
        });
    }

}
