package net.korex.gungame.netherchest;

import net.korex.gungame.Main;
import net.korex.gungame.Messages;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class NetherchestListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        NetherchestManager.getInstance().load(e.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Netherchest netherchest = NetherchestManager.getInstance().getFromCache(e.getPlayer());
        if(netherchest != null) {
            NetherchestManager.getInstance().write(netherchest);
        }
        NetherchestManager.getInstance().removeFromCache(e.getPlayer());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if(e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        if(e.getClickedBlock().getType() != Material.CHEST) {
            return;
        }

        e.setCancelled(true);
        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CHEST_OPEN,1f,1f);

        Netherchest netherchest = NetherchestManager.getInstance().getFromCache(e.getPlayer());
        if(netherchest == null || netherchest.getInventory() == null) {
            Messages.form(e.getPlayer(), "Deine Netherchest konnte §cnicht geladen werden§7! Bitte §crelogge oder warte §7um das Problem zu beheben!");
            return;
        }

        if (e.getPlayer().hasPermission(Main.PREMIUM)) {
            e.getPlayer().openInventory(netherchest.getInventory());
        } else {
            Messages.form(e.getPlayer(), "Um die Netherchest zu öffnen, brauchst du §cmindestens den Premium Rang§7!");
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent e) {
        if(!e.getInventory().getName().equals("Nether Chest")) {
            return;
        }

        Player p = (Player)e.getPlayer();
        Netherchest netherchest = NetherchestManager.getInstance().getFromCache(p);
        if(netherchest == null || netherchest.getInventory() == null) {
            return;
        }

        netherchest.save(e.getInventory());
        Messages.form(p, "Deine Netherchest §awurde gespeichert§7!");
    }


}
