package net.korex.gungame.netherchest;

import net.korex.bettersql.SqlColumn;
import net.korex.bettersql.SqlColumnType;
import net.korex.bettersql.SqlStatement;
import net.korex.bettersql.SqlTable;
import net.korex.gungame.stats.Stats;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class NetherchestManager {

    private static NetherchestManager instance = new NetherchestManager();

    public static NetherchestManager getInstance() {
        return instance;
    }

    private SqlTable table;
    private HashMap<Player, Netherchest> cache;

    private NetherchestManager() {
        this.table = new SqlTable("GunGame_Netherchest");
        this.cache = new HashMap<>();

        this.table.create(new SqlColumn("uuid", SqlColumnType.VARCHAR).setPrimary().setLength(64), new SqlColumn("content", SqlColumnType.TEXT));
    }

    public void loadAll() {
        for(Player player : Bukkit.getOnlinePlayers()) {
            this.load(player);
        }
    }

    public Netherchest getFromCache(Player player) {
        if(cache.containsKey(player)) {
            return cache.get(player);
        } else {
            return null;
        }
    }

    public void removeFromCache(Player player) {
        cache.remove(player);
    }

    public void load(Player player) {
        this.table.get("uuid=" + player.getUniqueId().toString()).async(sqlResultSet -> {
            if (sqlResultSet.size() == 0) {
                Netherchest netherchest = new Netherchest(player);
                this.cache.put(player, netherchest);
                return;
            }

            String content = sqlResultSet.get("content", 0).toString();
            Netherchest netherchest = new Netherchest(player);
            netherchest.loadInvFromBase64(content);
            this.cache.put(player, netherchest);
        });
    }

    public void write(Netherchest netherchest) {
        this.table.contains("uuid=" + netherchest.getPlayer().getUniqueId().toString()).async(aBoolean -> {
            String content = netherchest.invToBase64();

            if (aBoolean == null || !aBoolean) {
                SqlStatement stmt = new SqlStatement("INSERT INTO " + this.table.getName() + " (uuid, content) VALUES ('" + netherchest.getPlayer().getUniqueId().toString() + "', '" + content + "');", this.table);
                stmt.update();
                return;
            }
            SqlStatement stmt = new SqlStatement("UPDATE " + this.table.getName() + " SET content='" + content +"' WHERE uuid='" + netherchest.getPlayer().getUniqueId().toString() + "';", this.table);
            stmt.update();
        });
    }

}
