package net.korex.gungame.motd;

import net.korex.gungame.FileManager;
import org.bukkit.ChatColor;

public class MotdFile extends FileManager {

    public MotdFile() {
        super("motd");
    }

    public String getMotd() {
        this.reload();
        if(!this.getCfg().contains("content")) {
            return null;
        }
        String motd = this.getCfg().getString("content");
        if(motd == null) {
            return null;
        }
        motd = ChatColor.translateAlternateColorCodes('&', motd);
        return motd;
    }
}
