package net.korex.gungame.motd;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class MotdListener implements Listener {

    @EventHandler
    public void onServerListPing(ServerListPingEvent e) {
        String motd = new MotdFile().getMotd();
        if(motd != null) {
            e.setMotd(motd);
        }
    }
}
