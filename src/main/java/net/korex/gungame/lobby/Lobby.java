package net.korex.gungame.lobby;

import net.korex.gungame.Main;
import net.korex.gungame.Messages;
import net.korex.gungame.maps.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;

public class Lobby {

    private static HashMap<Player, BukkitTask> countdown = new HashMap<>();


    private Player p;
    private int count;

    public Lobby(Player p) {
        this.p = p;
        this.count = 5;
    }

    private void teleport() {
        MapManager.getInstance().removeFromCurrentMap(p);
        p.teleport(new LobbyLocation().getLocation());
        p.setHealth(20);
        p.setFireTicks(0);
        p.playSound(p.getLocation(), Sound.LEVEL_UP, 0.5f, 1.5f);
        Messages.form(p, "Du wurdest zur §aLobby §7teleportiert!");
    }

    public void start() {
        if (countdown.containsKey(this.p)) {
            Messages.form(p, "§cDu wirst bereits zur Lobby teleportiert!");
            return;
        }

        if(!MapManager.getInstance().inMap(p)) {
            this.teleport();
            return;
        }

        if(MapManager.getInstance().isPlayerInProtection(p)) {
            this.teleport();
            return;
        }

        BukkitTask task = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), () -> {
            count = count - 1;
            sendBar(count);
            p.getWorld().playSound(p.getLocation(), Sound.NOTE_BASS, 1f, 1f);

            if (count == 0) {
                teleport();
                countdown.get(p).cancel();
                countdown.remove(p);
            }

        }, 0, 20);

        countdown.put(this.p, task);
    }

    public void stop() {
        if (!countdown.containsKey(this.p)) {
            return;
        }

        countdown.get(this.p).cancel();
        countdown.remove(this.p);
        Messages.form(this.p, "§cDer Teleportvorgang wurde abgebrochen!");
        this.p.sendTitle("§8[§cXXXXX§8]", "§cTeleport abgebrochen");
    }


    private void sendBar(int count) {
        String s = "§8[";

        for (int i = 0; i < (5 - count); i++) {
            s = s + "§a■";
        }

        for (int i = 0; i < count; i++) {
            s = s + "§c■";
        }



        s = s + "§8]";

        p.sendTitle(s, "");
    }

}
