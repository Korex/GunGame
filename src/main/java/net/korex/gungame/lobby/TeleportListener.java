package net.korex.gungame.lobby;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class TeleportListener implements Listener{
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if(e.getFrom().getBlockX() != e.getTo().getBlockX() || e.getFrom().getBlockY() != e.getTo().getBlockY() || e.getFrom().getBlockZ() != e.getTo().getBlockZ()) {
			new Lobby(e.getPlayer()).stop();
		}
	}

	public void onDmg(EntityDamageEvent e) {
		if(e.getEntityType() == EntityType.PLAYER) {
			new Lobby((Player) e.getEntity()).stop();
		}
	}

}
