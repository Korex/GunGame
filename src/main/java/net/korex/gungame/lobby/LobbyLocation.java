package net.korex.gungame.lobby;

import org.bukkit.Location;

import net.korex.gungame.FileManager;
import net.korex.gungame.LocationConverter;

public class LobbyLocation extends FileManager{

	public LobbyLocation() {
		super("lobby");
	}
	
	public void setLobby(Location loc) {
		super.getCfg().set("Lobby.location", LocationConverter.convertToString(loc));
		super.save();
	}
	
	public Location getLocation() {
		String s = super.getCfg().getString("Lobby.location");
		Location loc = LocationConverter.convertFromString(s);
		return loc;
	}
	
}
