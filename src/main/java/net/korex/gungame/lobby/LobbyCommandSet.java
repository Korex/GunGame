package net.korex.gungame.lobby;

import net.korex.gungame.Messages;
import org.bukkit.entity.Player;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;

public class LobbyCommandSet extends AbstractCommand {

    public LobbyCommandSet() {
        super("setLobby");
    }

    @Override
    public void onExecute(Player p, String[] args) {
        LobbyLocation l = new LobbyLocation();
        l.setLobby(p.getLocation());
        Messages.form(p, "Du hast die §aLobby gesetzt§7!");
    }

    @Override
    public void sendHelp(Player p) {
        p.sendMessage(Main.PR + "§a/gg setLobby");

    }

}
