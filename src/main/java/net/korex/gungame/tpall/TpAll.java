package net.korex.gungame.tpall;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;
import net.korex.gungame.Messages;
import net.korex.gungame.lobby.LobbyLocation;
import net.korex.gungame.maps.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TpAll extends AbstractCommand {

	public TpAll() {
		super("tpall");
	}

	@Override
	public void onExecute(Player p, String[] args) {

		Location loc = new LobbyLocation().getLocation();

		for (Player players : Bukkit.getOnlinePlayers()) {
			if (MapManager.getInstance().inMap(players)) {
				MapManager.getInstance().removeFromCurrentMap(players);
			}

			players.teleport(loc);
			Messages.form(players, "Du wurdest zur §aLobby §7teleportiert!");

		}

		Messages.form(p, "Du hast alle Spieler zum §aSpawn§7 teleportiert!");
	}

	@Override
	public void sendHelp(Player p) {
		p.sendMessage(Main.PR + "§a/gg tpall");

	}

}
