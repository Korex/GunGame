package net.korex.gungame;

import net.korex.bettersql.SqlManager;
import net.korex.gungame.autorespawn.AutoRespawnListener;
import net.korex.gungame.broadcast.Broadcast;
import net.korex.gungame.discord.DiscordCommand;
import net.korex.gungame.inventory.AnvilCloseListener;
import net.korex.gungame.inventory.TrashCommand;
import net.korex.gungame.kits.kit.KitListener;
import net.korex.gungame.kits.kit.KitManager;
import net.korex.gungame.maps.MapManager;
import net.korex.gungame.motd.MotdListener;
import net.korex.gungame.netherchest.NetherchestListener;
import net.korex.gungame.netherchest.NetherchestManager;
import net.korex.gungame.rank.PlayerChatEvent;
import net.korex.gungame.rank.TablistTeamListener;
import net.korex.gungame.rank.TablistTeam;
import net.korex.gungame.shop.RepairSignListener;
import net.korex.gungame.specialitem.CoinVoucher;
import net.korex.gungame.specialitem.GoldenApple;
import net.korex.gungame.specialitem.Hoe;
import net.korex.gungame.stats.StatsManager;
import net.korex.gungame.tablist.TablistListener;
import net.korex.gungame.vote.VoteCommand;
import net.korex.gungame.vote.VoteListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import net.korex.gungame.anticheat.AntiCheatGamemodeCommand;
import net.korex.gungame.anticheat.AntiCheatGiveCommand;
import net.korex.gungame.anticheat.AntiCheatListener;
import net.korex.gungame.anticheat.AntiCheatXPCommand;
import net.korex.gungame.chatclear.ChatClear;
import net.korex.gungame.disallowedlisteners.DisallowedListeners;
import net.korex.gungame.extraxp.ExtraXpListener;
import net.korex.gungame.giveall.GiveAll;
import net.korex.gungame.information.DeniedCommands;
import net.korex.gungame.information.HilfeCommand;
import net.korex.gungame.information.RangCommand;
import net.korex.gungame.kits.kit.KitCommand;
import net.korex.gungame.kits.level.LevelCommand;
import net.korex.gungame.kits.level.LevelListener;
import net.korex.gungame.specialitem.EscapeString;
import net.korex.gungame.lobby.LobbyLocation;
import net.korex.gungame.lobby.LobbyCommand;
import net.korex.gungame.lobby.LobbyCommandSet;
import net.korex.gungame.lobby.TeleportListener;
import net.korex.gungame.maps.MapCommand;
import net.korex.gungame.maps.MapListener;
import net.korex.gungame.maps.WaterDead;
import net.korex.gungame.maps.signs.MapSignListener;
import net.korex.gungame.regeneration.RegenerationListener;
import net.korex.gungame.maps.regions.RegionCommandAdd;
import net.korex.gungame.maps.regions.RegionCommandSetPoints;
import net.korex.gungame.maps.regions.RegionListener;
import net.korex.gungame.reset.ResetCommand;
import net.korex.gungame.shop.ShopSignListener;
import net.korex.gungame.stats.StatsCommand;
import net.korex.gungame.stats.StatsEditCommand;
import net.korex.gungame.stats.StatsListener;
import net.korex.gungame.tpall.TpAll;
import net.korex.gungame.vanish.VanishCommand;

public class Main extends JavaPlugin {

    public static final String PR = "§7[§3Gun§7-§cGame§7]";
    public static final String ADMIN = "gungame.admin";
    public static final String PREMIUM = "gungame.premium";
    public static final String SUPREMIUM = "gungame.supremium";
    public static final String TEAM = "gungame.team";

    private static Main m;

    public static Main getInstance() {
        return m;
    }


    @Override
    public void onLoad() {
        m = this;
    }

    @Override
    public void onEnable() {

        super.getCommand("gungame").setExecutor(new GunGameCommand());
        super.getCommand("spawn").setExecutor(new LobbyCommand());
        super.getCommand("stats").setExecutor(new StatsCommand());
        super.getCommand("kit").setExecutor(new KitCommand());
        super.getCommand("vanish").setExecutor(new VanishCommand());
        super.getCommand("chatclear").setExecutor(new ChatClear());
        super.getCommand("Rang").setExecutor(new RangCommand());
        super.getCommand("trash").setExecutor(new TrashCommand());
        super.getCommand("hilfe").setExecutor(new HilfeCommand());
        super.getCommand("discord").setExecutor(new DiscordCommand());
        super.getCommand("vote").setExecutor(new VoteCommand());

        super.getCommand("gamemode").setExecutor(new AntiCheatGamemodeCommand());
        super.getCommand("give").setExecutor(new AntiCheatGiveCommand());
        super.getCommand("xp").setExecutor(new AntiCheatXPCommand());


        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new LevelListener(), this);
        pm.registerEvents(new DisallowedListeners(), this);
        pm.registerEvents(new MapSignListener(), this);
        pm.registerEvents(new WaterDead(), this);
        pm.registerEvents(new MapListener(), this);
        pm.registerEvents(new TeleportListener(), this);
        pm.registerEvents(new StatsListener(), this);
        pm.registerEvents(new ShopSignListener(), this);
        pm.registerEvents(new EscapeString(), this);
        pm.registerEvents(new Hoe(), this);
        pm.registerEvents(new GoldenApple(), this);
        pm.registerEvents(new RegionListener(), this);
        pm.registerEvents(new RegenerationListener(), this);
        pm.registerEvents(new ExtraXpListener(), this);
        pm.registerEvents(new DeniedCommands(), this);
        pm.registerEvents(new AntiCheatListener(), this);
        pm.registerEvents(new RepairSignListener(), this);
        pm.registerEvents(new PlayerChatEvent(), this);
        pm.registerEvents(new TablistTeamListener(), this);
        pm.registerEvents(new KitListener(), this);
        pm.registerEvents(new TablistListener(), this);
        pm.registerEvents(new VoteListener(), this);
        pm.registerEvents(new AnvilCloseListener(), this);
        pm.registerEvents(new CoinVoucher(), this);
        pm.registerEvents(new AutoRespawnListener(), this);
        pm.registerEvents(new NetherchestListener(), this);
        pm.registerEvents(new MotdListener(), this);

        new LevelCommand().create();
        new MapCommand().create();
        new LobbyCommandSet().create();
        new GiveAll().create();
        new StatsEditCommand().create();
        new TpAll().create();
        new ResetCommand().create();
        new RegionCommandAdd().create();
        new RegionCommandSetPoints().create();

        TablistTeam.registerStandardTeams();
        TablistTeam.addAll();
        SqlManager.getInstance().connect("localhost", "root", "password", "gungame");
        StatsManager.getInstance().loadAll();
        KitManager.getInstance().loadAll();
        NetherchestManager.getInstance().loadAll();
        Broadcast.start();

        Bukkit.broadcastMessage(Main.PR + "§aDas GunGame Plugin wurde gestartet!");
    }

    @Override
    public void onDisable() {

        // tp all to spawn
        Location loc = new LobbyLocation().getLocation();

        for (Player players : Bukkit.getOnlinePlayers()) {
            if (MapManager.getInstance().inMap(players)) {
                MapManager.getInstance().removeFromCurrentMap(players);
            }

            players.teleport(loc);
            Messages.form(players, "Du wurdest wegen eines §aServerreloads §7zur §aLobby §7teleportiert!");
        }

    }

}
