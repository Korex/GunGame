package net.korex.gungame.specialitem;

import net.korex.gungame.Messages;
import net.korex.gungame.lobby.LobbyLocation;
import net.korex.gungame.maps.MapManager;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class EscapeString implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {

            if (e.getPlayer().getItemInHand().getType() == Material.NAME_TAG) {
                if (MapManager.getInstance().inMap(e.getPlayer())) {
                    if (e.getPlayer().getItemInHand().getAmount() > 1) {
                        e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
                    } else {
                        e.getPlayer().getInventory().remove(e.getPlayer().getItemInHand());
                    }


                    MapManager.getInstance().removeFromCurrentMap(e.getPlayer());
                    e.getPlayer().teleport(new LobbyLocation().getLocation());

                    Messages.form(e.getPlayer(), "Du hast ein §aFluchtseil§7 benutzt und wurdest zur §aLobby §7teleportiert!");
                } else {
                    Messages.form(e.getPlayer(), "Du darfst hier §ckeine Fluchtseile§7 verwenden!");

                }

            }

        }
    }

}
