package net.korex.gungame.specialitem;

import net.korex.gungame.stats.Stats;
import net.korex.gungame.stats.StatsManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class CoinVoucher implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if(e.getItem() == null) {
                return;
            }
            if (e.getItem().getType() == Material.PAPER) {
                ItemStack item = e.getItem();
                //§aGutschein: 50 Coins
                System.out.println(item.getItemMeta().getDisplayName());
                if (item.getItemMeta().getDisplayName().contains("Gutschein:")) {
                    String name = item.getItemMeta().getDisplayName();
                    String[] words = name.split(" ");
                    try {
                        int coins = Integer.parseInt(words[1]);
                        Stats stats = StatsManager.getInstance().getFromCache(e.getPlayer());
                        if (stats != null) {
                            stats.setCoins(stats.getCoins() + coins);
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.LEVEL_UP, 1f, 1f);
                            if (e.getItem().getAmount() == 1) {
                                e.getPlayer().getInventory().remove(item);
                            } else {
                                e.getPlayer().getInventory().remove(item);
                                item.setAmount(item.getAmount() - 1);
                                e.getPlayer().getInventory().setItemInHand(item);
                            }
                        }
                    } catch (NumberFormatException e2) {
                    }
                }
            }
        }
    }
}
