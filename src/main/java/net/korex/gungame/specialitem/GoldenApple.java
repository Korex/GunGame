package net.korex.gungame.specialitem;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class GoldenApple implements Listener {

    @EventHandler
    public void onEar(PlayerItemConsumeEvent e) {
        if (e.getItem().getType() != Material.GOLDEN_APPLE) {
            return;
        }

        e.setCancelled(true);


        byte data = e.getItem().getData().getData();

        if (data == (byte) 0) {
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 20, 4), true);
        } else {
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 60, 4), true);
        }

        if(e.getPlayer().getItemInHand().getAmount() == 1) {
            e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
            e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.ENDERDRAGON_GROWL, 0.3f, 2f);

        } else {
            ItemStack is = e.getPlayer().getItemInHand();
            is.setAmount(is.getAmount() -1);
            e.getPlayer().setItemInHand(is);
            e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.ENDERDRAGON_GROWL, 1f, 2f);

        }
    }
}
