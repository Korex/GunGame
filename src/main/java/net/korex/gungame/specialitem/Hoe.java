package net.korex.gungame.specialitem;

import net.korex.gungame.maps.MapManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Hoe implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {

            if (e.getPlayer().getItemInHand().getType() == Material.IRON_HOE) {

                Player player = e.getPlayer();

                if(!MapManager.getInstance().inMap(player) || MapManager.getInstance().isPlayerInProtection(e.getPlayer())) {
                    return;
                }

                short durability = (short) (player.getItemInHand().getDurability() + 5) ;

                if(durability >= 250) {
                    player.playSound(player.getLocation(), Sound.ITEM_BREAK, 1f,1f);
                    return;
                } else {
                    ItemStack itemStack = player.getItemInHand();
                    itemStack.setDurability(durability);
                    player.getInventory().setItemInHand(itemStack);

                    Arrow arrow = player.launchProjectile(Arrow.class, player.getLocation().getDirection());
                    arrow.setShooter(player);
                    arrow.setVelocity(player.getLocation().getDirection().multiply(4.0));
                    arrow.setCritical(true);
                    player.getWorld().playSound(player.getLocation(), Sound.ITEM_BREAK, 0.5f, 0.5f);
                }
            }

        }
    }

}
