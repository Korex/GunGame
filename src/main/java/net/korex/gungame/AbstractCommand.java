package net.korex.gungame;

import java.util.HashMap;

import org.bukkit.entity.Player;

public abstract class AbstractCommand {

	private static HashMap<String, AbstractCommand> subCommands;

	private String subCommand;

	public AbstractCommand(String subCommand) {
		this.subCommand = subCommand;
	}

	static {
		subCommands = new HashMap<>();
	}
	
	public void create() {
		subCommands.put(this.subCommand.toLowerCase(), this);
	}
	
	public static boolean containsCommand(String command) {
		return subCommands.containsKey(command);
	}
	
	public static AbstractCommand getCommand(String command) {
		return subCommands.get(command);
	}
	
	public static HashMap<String, AbstractCommand> getSubCommands() {
		return subCommands;
	}

	public abstract void onExecute(Player p, String[] args);

	public abstract void sendHelp(Player p);

}
