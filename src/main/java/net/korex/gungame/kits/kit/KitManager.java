package net.korex.gungame.kits.kit;

import net.korex.bettersql.SqlColumn;
import net.korex.bettersql.SqlColumnType;
import net.korex.bettersql.SqlTable;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KitManager {

    private static KitManager instance = new KitManager();

    public static KitManager getInstance() {
        return instance;
    }

    private SqlTable table;

    private HashMap<Player, List<KitCache>> cache;

    private KitManager() {
        this.table = new SqlTable("GunGame_Kits");
        this.table.create(new SqlColumn("id", SqlColumnType.BIGINT).setPrimary().setAutoIncrement(), new SqlColumn("uuid", SqlColumnType.VARCHAR).setLength(64), new SqlColumn("kitId", SqlColumnType.VARCHAR).setLength(10), new SqlColumn("lastUsed", SqlColumnType.BIGINT));
        this.cache = new HashMap<>();
    }

    public void loadAll() {
        Bukkit.getOnlinePlayers().forEach((player) -> {
            this.load(player);
        });
    }

    public KitCache getFromCache(Player player, Kit kit) {
        List<KitCache> cacheList = getFromCache(player);
        if (cacheList != null) {
            for (KitCache kitCache : getFromCache(player)) {
                if (kitCache.getKit().getId().equalsIgnoreCase(kit.getId())) {
                    return kitCache;
                }
            }
        }

        KitCache cache = new KitCache(0, kit);
        if(cacheList == null) {
            cacheList = new ArrayList<>();
        }
        cacheList.add(cache);
        this.cache.put(player, cacheList);

        return cache;
    }

    public List<KitCache> getFromCache(Player player) {
        return this.cache.get(player);
    }

    public void removeFromCache(Player player) {
        this.cache.remove(player);
    }

    public void load(Player player) {
        this.table.get("uuid=" + player.getUniqueId().toString()).async(sqlResultSet -> {
            if (sqlResultSet.size() == 0) {
                this.cache.put(player, new ArrayList<>());
                return;
            }

            List<KitCache> list = new ArrayList<>();
            for (int i = 0; i < sqlResultSet.size(); i++) {
                Long lastUsed = (Long) sqlResultSet.get("lastUsed", i);
                String kitId = sqlResultSet.get("kitId", i).toString();
                Kit kit = Kit.getKitById(kitId);
                KitCache kitCache = new KitCache(lastUsed, kit);
                list.add(kitCache);
            }

            this.cache.put(player, list);
        });
    }

    public void write(Player player) {
        List<KitCache> list = this.getFromCache(player);
        if (list == null) {
            return;
        }

        for (KitCache kit : list) {
            this.table.contains("uuid=" + player.getUniqueId().toString() + ";kitId=" + kit.getId()).async(aBoolean -> {
                if (aBoolean == null || !aBoolean) {
                    this.table.insert("uuid=" + player.getUniqueId().toString() + ";kitId=" + kit.getId() + ";lastUsed=" + kit.getLastUsed()).sync();
                } else {
                    this.table.update("lastUsed=" + kit.getLastUsed(), "uuid=" + player.getUniqueId().toString() + ";kitId=" + kit.getId()).sync();
                }
            });
        }
    }


}
