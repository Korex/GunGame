package net.korex.gungame.kits.kit;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;

public class KitListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        KitManager.getInstance().load(e.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        List<KitCache> cache = KitManager.getInstance().getFromCache(e.getPlayer());
        if (cache != null) {
            KitManager.getInstance().write(e.getPlayer());
        }
        KitManager.getInstance().removeFromCache(e.getPlayer());
    }

}
