package net.korex.gungame.kits.kit;

import net.korex.gungame.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.korex.gungame.Main;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class KitCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }
        Player p = (Player) sender;
        if(args.length != 1) {
            Messages.form(p, "§cBenutze: /kit <Standard|Special|Premium>");
            return true;
        }

         Kit kit = Kit.getKitById(args[0].toUpperCase());
        if(kit == null) {
            Messages.form(p, "§cBenutze: /kit <Standard|Special|Premium>");
            return true;
        }

        KitCache cache = KitManager.getInstance().getFromCache(p, kit);

        if(cache == null) {
            Messages.form(p, "§cBitte logge dich aus und wieder ein. Die Kits konnten bei dir nicht geladen werden!");
            return true;
        }

        if(kit.getPermission() != null && !p.hasPermission(kit.getPermission())) {
            Messages.form(p, "§cDu darfst dieses Kit nicht benutzen!");
            return true;
        }

        if (cache.isCooldownOver()) {

            for (ItemStack item : cache.getKit().getItems()) {
                p.getInventory().addItem(item);
            }

            cache.setLastUsed();

            Messages.form(p, "Du hast dein Kit §aerhalten§7!");
        } else {
            Messages.form(p, "Die Zeit ist noch nicht um! Warte noch §c" + cache.getTimeAsString() + " §7bis du das Kit benutzen kannst!");
        }

        return false;
    }


}
