package net.korex.gungame.kits.kit;

public class KitCache {

    private long lastUsed;
    private String id;
    private Kit kit;

    public KitCache(long lastUsed, Kit kit) {
        this.lastUsed = lastUsed;
        this.id = kit.getId();
        this.kit = kit;
    }

    public String getId() {
        return id;
    }

    public long getLastUsed() {
        return lastUsed;
    }

    public Kit getKit() {
        return kit;
    }

    public boolean isCooldownOver() {
        long cooldownInMillis = kit.getCooldownHours() * 1000 * 60 * 60;
        cooldownInMillis = lastUsed + cooldownInMillis;
        return System.currentTimeMillis() > (cooldownInMillis);
    }

    public String getTimeAsString() {
        long past = System.currentTimeMillis() - lastUsed;

        int hours = this.kit.getCooldownHours() * 60 * 60 * 1000;
        past = hours - past;

        if (past > (60 * 60 * 1000 * 24)) {
            double d = past;
            d = d / 24 / 60 / 60 / 1000;
            d = d * 10;
            d = Math.round(d);
            d = d / 10;

            return d + " Tage";
        } else if (past > (60 * 60 * 1000)) {
            double d = past;
            d = d / 60 / 60 / 1000;
            d = d * 10;
            d = Math.round(d);
            d = d / 10;

            return d + " Stunden";
        } else if (past < (1000 * 60)) {
            return past / 1000 + " Sekunden";
        } else {
            return past / 60 / 1000 + " Minuten";
        }
    }

    public void setLastUsed() {
        this.lastUsed = System.currentTimeMillis();
    }


}
