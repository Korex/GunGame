package net.korex.gungame.kits.kit;

import net.korex.gungame.Main;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Kit {

    static class CustomEnchantment {

        private Enchantment e;
        private int strength;

        CustomEnchantment(Enchantment e, int strength) {
            this.e = e;
            this.strength = strength;
        }

        public Enchantment getE() {
            return e;
        }

        public int getStrength() {
            return strength;
        }

    }

    private static HashMap<String, Kit> kitList = new HashMap<>();

    public static Kit getKitById(String id) {
        if(!kitList.containsKey(id)) {
            return null;
        }
        return kitList.get(id);
    }

    static {
        kitList.put("STANDARD", getStandardKit());
        kitList.put("SPECIAL", getSpecialKit());
        kitList.put("PREMIUM", getPremiumKit());
        kitList.put("VOTE", getVoteKit());
    }

    private static final Kit getVoteKit() {
        List<ItemStack> items = new ArrayList<>();
        items.add(getStack(Material.PAPER, new ArrayList<>(), "§aGutschein: 25 Coins",1));
        Kit kit = new Kit("VOTE", 0, Main.ADMIN, items);
        return kit;
    }

    private static final Kit getStandardKit() {
        List<CustomEnchantment> armor = new ArrayList<CustomEnchantment>();
        armor.add(new CustomEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2));
        armor.add(new CustomEnchantment(Enchantment.DURABILITY, 1));

        List<CustomEnchantment> sword = new ArrayList<CustomEnchantment>();
        sword.add(new CustomEnchantment(Enchantment.DAMAGE_ALL, 2));
        sword.add(new CustomEnchantment(Enchantment.DURABILITY, 1));

        List<ItemStack> items = new ArrayList<>();


        items.add(getStack(Material.IRON_BOOTS, armor));
        items.add(getStack(Material.IRON_LEGGINGS, armor));
        items.add(getStack(Material.IRON_CHESTPLATE, armor));
        items.add(getStack(Material.IRON_HELMET, armor));
        items.add(getStack(Material.IRON_SWORD, sword));

        items.add(getStack(Material.IRON_HOE, new ArrayList<>(), "§aMG", 1));
        items.add(getStack(Material.ENDER_PEARL, new ArrayList<>(), "§aE-Perle", 4));

        return new Kit("STANDARD", 24, null, items);
    }

    private static final Kit getSpecialKit() {
        List<ItemStack> items = new ArrayList<>();

        items.add(getStack(Material.DIAMOND, new ArrayList<>(), null, 3));
        items.add(getStack(Material.ENDER_PEARL, new ArrayList<>(), "§aE-Perle", 6));
        items.add(getStack(Material.IRON_HOE, new ArrayList<>(), "§aMG", 1));
        items.add(getStack(Material.IRON_HOE, new ArrayList<>(), "§aMG", 1));
        items.add(getStack(Material.EXP_BOTTLE, new ArrayList<>(), null, 32));
        items.add(getStack(Material.GOLDEN_APPLE, new ArrayList<>(), "§aGapple", 5));
        items.add(getStack(Material.PAPER, new ArrayList<>(), "§aGutschein: 50 Coins",1));

        return new Kit("SPECIAL", 72, null, items);
    }

    private static final Kit getPremiumKit() {
        List<ItemStack> items = new ArrayList<>();

        items.add(getStack(Material.DIAMOND, new ArrayList<>(), null, 3));
        items.add(getStack(Material.ENDER_PEARL, new ArrayList<>(), "§aE-Perle", 6));
        items.add(getStack(Material.GOLDEN_APPLE, new ArrayList<>(), "§aGapple", 8));
        items.add(getStack(Material.NAME_TAG, new ArrayList<>(), "§aFluchtseil", 1));
        items.add(getStack(Material.EXP_BOTTLE, new ArrayList<>(), null, 64));
        items.add(getStack(Material.PAPER, new ArrayList<>(), "§aGutschein: 50 Coins",1));

        return new Kit("PREMIUM", 72, Main.PREMIUM, items);
    }

    private static ItemStack getStack(Material m, List<CustomEnchantment> enchantments, String name, int amount) {
        ItemStack item = new ItemStack(m);
        ItemMeta meta = item.getItemMeta();
        if (name != null) {
            meta.setDisplayName(name);
        }
        for (CustomEnchantment enchantment : enchantments) {
            meta.addEnchant(enchantment.getE(), enchantment.getStrength(), true);
        }
        item.setItemMeta(meta);
        item.setAmount(amount);
        return item;
    }

    private static ItemStack getStack(Material m, List<CustomEnchantment> enchantments) {
        return getStack(m, enchantments, null, 1);
    }

    private String id;
    private List<ItemStack> items;
    private int cooldownHours;
    private String permission;

    public Kit(String id, int cooldownHours, String permission, List<ItemStack> items) {
        this.id = id;
        this.items = items;
        this.cooldownHours = cooldownHours;
        this.permission = permission;
    }

    public int getCooldownHours() {
        return cooldownHours;
    }

    public String getId() {
        return id;
    }

    public List<ItemStack> getItems() {
        return items;
    }

    public String getPermission() {
        return permission;
    }
}
