package net.korex.gungame.kits.level;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;

public class LevelListener implements Listener{
	@EventHandler
	public void onLevelChange(PlayerLevelChangeEvent e) {
		Level k = new Level();
		k.removeKit(e.getPlayer());
		k.setKit(e.getPlayer(), e.getNewLevel());
		e.getPlayer().updateInventory();
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		if(e.getPlayer().hasPlayedBefore()) {
			return;
		}

		e.getPlayer().setLevel(20);
		Level l = new Level();
		l.setKit(e.getPlayer(),20);
		e.getPlayer().updateInventory();
	}

}
