package net.korex.gungame.kits.level;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;

public class LevelCommand extends AbstractCommand {

	public LevelCommand() {
		super("setlevel");
	}

	@Override
	public void onExecute(Player p, String[] args) {
		if (args.length == 2) {

			if (args[1].equalsIgnoreCase("end")) {
				Level lvl = new Level();
				List<ItemStack> l = new ArrayList<ItemStack>();

				for (ItemStack item : p.getInventory().getContents()) {
					l.add(item);
				}

				for (ItemStack item : p.getInventory().getArmorContents()) {
					l.add(item);
				}
				lvl.deleteEndKit();
				lvl.setEndLevel(l);

				p.sendMessage(Main.PR + " Du hast das §aEnd-Kit§7 gesetzt!");

			} else {
				try {
					int i = Integer.parseInt(args[1]);

					Level k = new Level();
					k.deleteKit(i);

					List<ItemStack> l = new ArrayList<ItemStack>();

					for (ItemStack item : p.getInventory().getContents()) {
						l.add(item);
					}

					for (ItemStack item : p.getInventory().getArmorContents()) {
						l.add(item);
					}

					k.setKit(i, l);

					p.sendMessage(Main.PR + " Du hast das Kit für Lvl §a" + i + "§7 gesetzt!");

				} catch (NumberFormatException e) {
					sendHelp(p);
				}
			}

		} else {
			sendHelp(p);
		}

	}

	@Override
	public void sendHelp(Player p) {
		p.sendMessage(Main.PR + "§a/gg setlevel <lvl/end>");

	}

}
