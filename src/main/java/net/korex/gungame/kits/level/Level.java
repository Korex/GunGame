package net.korex.gungame.kits.level;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.korex.gungame.FileManager;
import net.korex.gungame.Main;

public class Level extends FileManager {

	public Level() {
		super("level");
	}

	public void setKit(int lvl, List<ItemStack> contents) {

		for (int i = 0; i < contents.size(); i++) {

			ItemStack item = contents.get(i);
			
			if (item != null && item.getType() != Material.AIR) {

				super.getCfg().set("level." + lvl + "." + i, item);

			}

		}

		super.save();
	}
	
	public void setEndLevel(List<ItemStack> contents) {
		for (int i = 0; i < contents.size(); i++) {

			ItemStack item = contents.get(i);
			
			if (item != null && item.getType() != Material.AIR) {

				super.getCfg().set("level.end." + i, item);

			}

		}

		super.save();
	}

	public void removeKit(Player p) {
		for (int i = 0; i < 40; i++) {
			ItemStack item = p.getInventory().getItem(i);
			if (item != null && item.getType() != Material.AIR && item.hasItemMeta() && item.getItemMeta().hasLore()) {

				List<String> l = item.getItemMeta().getLore();

				if (l.get(0).equals(Main.PR)) {
					p.getInventory().setItem(i, new ItemStack(Material.AIR));
				}

			}
		}
		p.updateInventory();

	}

	public void setKit(Player p, int lvl) {

		if (super.getCfg().contains("level." + lvl)) {
			for (int i = 0; i < 40; i++) {
				ItemStack item = super.getCfg().getItemStack("level." + lvl + "." + i);

				if (item != null && item.getType() != Material.AIR) {

					ItemMeta meta = item.getItemMeta();
					meta.setDisplayName("§a" + item.getType().toString());
					ArrayList<String> lore = new ArrayList<>();
					lore.add(Main.PR);
					meta.setLore(lore);
					item.setItemMeta(meta);

					if (p.getInventory().getItem(i) == null || p.getInventory().getItem(i).getType() == Material.AIR) {
						p.getInventory().setItem(i, item);
					} else {
						p.getInventory().addItem(item);
					}

				}
			}
		} else {
			for (int i = 0; i < 40; i++) {
				ItemStack item = super.getCfg().getItemStack("level.end." + i);

				if (item != null && item.getType() != Material.AIR) {

					ItemMeta meta = item.getItemMeta();
					meta.setDisplayName("§a" + item.getType().toString());
					ArrayList<String> lore = new ArrayList<>();
					lore.add(Main.PR);
					meta.setLore(lore);
					item.setItemMeta(meta);

					if (p.getInventory().getItem(i) == null || p.getInventory().getItem(i).getType() == Material.AIR) {
						p.getInventory().setItem(i, item);
					} else {
						p.getInventory().addItem(item);
					}

				}
			}
		}

	}

	public void deleteKit(int lvl) {
		super.getCfg().set("level." + lvl, null);
		super.save();
	}
	
	public void deleteEndKit() {
		if(super.getCfg().contains("level.end")) {
			super.getCfg().set("level.end", null);
			super.save();
		}
	}

}
