package net.korex.gungame.maps.regions;

import java.util.HashMap;

import net.korex.gungame.Messages;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;

public class RegionCommandSetPoints extends AbstractCommand {

	private static HashMap<Player, Location> loc1;
	private static HashMap<Player, Location> loc2;

	static {
		loc1 = new HashMap<>();
		loc2 = new HashMap<>();
	}
	
	public static Location getLoc1(Player p) {
		if(loc1.containsKey(p)) {
			return loc1.get(p);
		}
		return null;
	}
	
	public static Location getLoc2(Player p) {
		if(loc2.containsKey(p)) {
			return loc2.get(p);
		}
		return null;
	}

	public RegionCommandSetPoints() {
		super("setPoint");
	}

	@Override
	public void onExecute(Player p, String[] args) {
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("setpoint")) {
				if (args[1].equalsIgnoreCase("1")) {
					loc1.put(p, p.getLocation());
					Messages.form(p, "Du hast die §a1te§7 Position gesetzt!");
				} else if (args[1].equalsIgnoreCase("2")) {
					loc2.put(p, p.getLocation());
					Messages.form(p, "Du hast die §a2te§7 Position gesetzt!");
				} else {
					sendHelp(p);
				}
			} else {
				sendHelp(p);
			}
		} else {
			sendHelp(p);
		}
	}

	@Override
	public void sendHelp(Player p) {
		p.sendMessage(Main.PR + "§a/gg setpoint <1/2>");

	}

}
