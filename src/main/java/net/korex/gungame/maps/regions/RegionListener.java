package net.korex.gungame.maps.regions;

import net.korex.gungame.Messages;
import net.korex.gungame.maps.MapManager;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerFishEvent;

public class RegionListener implements Listener {

    //X in Spawn Protection, Y draußen, X kann Y abschießen

    @EventHandler
    public void onBow(EntityShootBowEvent e) {
        if(!(e.getEntity() instanceof Player)) {
            return;
        }


        if(MapManager.getInstance().isPlayerInProtection((Player) e.getEntity())) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof Player) || !(e.getEntity() instanceof Player)) {
            if (e.getDamager() instanceof Arrow) {
                Player p = (Player) e.getEntity();

                if(MapManager.getInstance().isPlayerInProtection(p)) {
                    e.setCancelled(true);
                    return;
                }
            }
            return;
        }

        Player damager = (Player) e.getDamager();

        if(MapManager.getInstance().isPlayerInProtection(damager)) {
            e.setCancelled(true);
            Messages.form(damager, "Du darfst am Spawn §cniemanden §7schlagen!");
            return;
        }

        Player p = (Player) e.getEntity();

        if(MapManager.getInstance().isPlayerInProtection(p)) {
            e.setCancelled(true);
            Messages.form(damager, "Du darfst am Spawn §cniemanden §7schlagen!");
            return;
        }

    }

    @EventHandler
    public void onFish(PlayerFishEvent e) {
        if (e.getCaught() instanceof Player) {
            Player p = e.getPlayer();

            if(MapManager.getInstance().isPlayerInProtection(p)) {
                e.setCancelled(true);
                Messages.form(p, "Du darfst am Spawn §cniemanden §7schlagen!");
                return;
            }

            Player caught = (Player) e.getCaught();
            if(MapManager.getInstance().isPlayerInProtection(caught)) {
                e.setCancelled(true);
                Messages.form(p, "Du darfst am Spawn §cniemanden §7schlagen!");
                return;
            }
        }
    }

    @EventHandler
    public void onDmg(EntityDamageEvent e) {
        if(e.getEntityType() == EntityType.PLAYER) {
            if(MapManager.getInstance().isPlayerInProtection((Player) e.getEntity())) {
                e.setCancelled(true);
            }
        }

    }

}
