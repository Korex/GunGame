package net.korex.gungame.maps.regions;

import net.korex.gungame.Messages;
import org.bukkit.entity.Player;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;
import net.korex.gungame.maps.Map;
import net.korex.gungame.maps.MapManager;

public class RegionCommandAdd extends AbstractCommand {

	public RegionCommandAdd() {
		super("addregion");
	}

	@Override
	public void onExecute(Player p, String[] args) {

		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("addregion")) {
				Map m = MapManager.getInstance().getMap(args[1]);
				if (m != null) {

					if(RegionCommandSetPoints.getLoc1(p) != null && RegionCommandSetPoints.getLoc2(p) != null) {
						m.setSpawnRegion(RegionCommandSetPoints.getLoc1(p), RegionCommandSetPoints.getLoc2(p));
						m.saveToConfig();
						Messages.form(p, "Du hast die Region §ahinzugefügt§7!");
					} else {
						Messages.form(p, "Du musst erst zwei Positionen §cfestlegen§7!");
					}

				} else {
					Messages.form(p, "Die Map existiert §cnicht§7!");
				}
			} else {
				sendHelp(p);
			}
		} else {
			sendHelp(p);
		}

	}

	@Override
	public void sendHelp(Player p) {
		p.sendMessage(Main.PR + "§a/gg addregion <MapName>");

	}

}
