package net.korex.gungame.maps;

import net.korex.gungame.Messages;
import org.bukkit.entity.Player;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;

public class MapCommand extends AbstractCommand {

	public MapCommand() {
		super("map");
	}

	@Override
	public void onExecute(Player p, String[] args) {
		if (args.length == 4) {
			if (args[1].equalsIgnoreCase("create")) {
				try {
					int i = Integer.parseInt(args[3]);
					String mapName = args[2];

					Map m = new Map(mapName, i, p.getLocation(), null, null, null);
					MapManager.getInstance().save(m);

					Messages.form(p, "Du hast eine Map §ahinzugefügt§7!");

				} catch (NumberFormatException e) {
					sendHelp(p);
				}
			}

		} else if (args.length == 3) {

			if (args[1].equalsIgnoreCase("remove")) {
				String mapName = args[2];

				Map m = MapManager.getInstance().getMap(mapName);

				if (m != null) {
					MapManager.getInstance().remove(m);
					Messages.form(p, "Du hast eine Map §cgelöscht§7!");
				} else {
					Messages.form(p, "Die Map existiert §cnicht§7!");
				}
			} else {
				sendHelp(p);
			}

		} else {
			sendHelp(p);
		}

	}

	@Override
	public void sendHelp(Player p) {
		p.sendMessage(Main.PR + "§a/gg map create <Name> <MaxPlayers>");
		p.sendMessage(Main.PR + "§a/gg map remove <Name>");
	}

}
