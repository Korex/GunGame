package net.korex.gungame.maps;

import net.korex.gungame.Main;
import net.korex.gungame.stats.Stats;
import net.korex.gungame.stats.StatsManager;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class WaterDead implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getTo().getBlock().getType() == Material.STATIONARY_WATER || e.getTo().getBlock().getType() == Material.WATER) {
            if (e.getPlayer().getGameMode() == GameMode.SURVIVAL || e.getPlayer().getGameMode() == GameMode.ADVENTURE) {
                if (!e.getPlayer().isDead()) {
                    if (!MapManager.getInstance().inMap(e.getPlayer())) {
                        return;
                    }

                    e.getPlayer().setHealth(0);
                    e.getPlayer().sendMessage(Main.PR + " Du wurdest von §cWASSER §7getötet!");
                    e.getPlayer().sendMessage(Main.PR + " §c-1§b Coin§7!");
                    Stats stats = StatsManager.getInstance().getFromCache(e.getPlayer());
                    if (stats == null) {
                        return;
                    }
                    stats.setCoins(stats.getCoins() - 1);
                    stats.addDeath();
                }

            }
        }
    }

}
