package net.korex.gungame.maps.signs;

import org.bukkit.Location;
import org.bukkit.block.Sign;

import net.korex.gungame.Main;

public class MapSign {

	private Sign s;
	
	private static String pr;

	public MapSign(Location signLoc) {
		if (signLoc.getBlock().getState() instanceof Sign) {
			Sign locSign = (Sign) signLoc.getBlock().getState();

			if (locSign.getLine(0).equals(Main.PR)) {
				s = locSign;
			}
		}
	}
	
	static {
		pr = Main.PR;
	}

	public static String getPrefix() {
		return MapSign.pr;
	}

	public void setPlayer(int player) {
		if (s != null) {
			String newLine = this.convertToMaxAndCurrentPlayers(player, this.getMaxPlayersOnSign());
			s.setLine(2, newLine);
			s.update();
		} 

	}
	
	public int getCurrentPlayersOnSign() {
		if(s != null) {
			String oldLine = s.getLine(2).replace("§b", "").replace("§c", "").replace("§7", "");
			String[] oldLineArray = oldLine.split("/");
			int currentPlayers = Integer.parseInt(oldLineArray[0]);
			return currentPlayers;
			
		}
		return 0;
	}
	
	public int getMaxPlayersOnSign() {
		if(s != null) {
			String oldLine = s.getLine(2).replace("§b", "").replace("§c", "").replace("§7", "");
			String[] oldLineArray = oldLine.split("/");
			int maxPlayers = Integer.parseInt(oldLineArray[1]);
			return maxPlayers;
			
		}
		return 0;
	}
	
	public String convertToMaxAndCurrentPlayers(int currentPlayers, int maxPlayers) {
		String s = "§b"+currentPlayers+"§7/§c"+maxPlayers;
		return s;
	}

}
