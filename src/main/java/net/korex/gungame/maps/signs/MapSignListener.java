package net.korex.gungame.maps.signs;

import net.korex.gungame.Messages;
import net.korex.gungame.maps.Map;
import net.korex.gungame.maps.MapManager;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class MapSignListener implements Listener {

	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("GunGame-Map")) {
			if (e.getLine(1) != null && !e.getLine(1).isEmpty()) {
				Map m = MapManager.getInstance().getMap(e.getLine(1));
				if (m != null) {
					e.setLine(0, MapSign.getPrefix());
					e.setLine(2, new MapSign(e.getBlock().getLocation()).convertToMaxAndCurrentPlayers(0, m.getMaxPlayers()));
					e.setLine(3, "§a:Ready");
					m.setSign(e.getBlock().getLocation());
					m.saveToConfig();
				} else {
					e.setCancelled(true);
				}
			}

		}
	}

	@EventHandler
	public void onInteractAtSign(PlayerInteractEvent e) {

		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getClickedBlock().getState() instanceof Sign) {
				Sign s = (Sign) e.getClickedBlock().getState();
				if (s.getLine(0).equalsIgnoreCase(MapSign.getPrefix())) {
					Map m = MapManager.getInstance().getMap(s.getLine(1));

					MapSign mapSign = new MapSign(e.getClickedBlock().getLocation());

					if(MapManager.getInstance().inMap(e.getPlayer())) {
						MapManager.getInstance().removeFromCurrentMap(e.getPlayer());
					}

					if(m.getCurrentPlayers() < m.getMaxPlayers()) {
						m.setCurrentPlayers(m.getCurrentPlayers() + 1);
						mapSign.setPlayer(m.getCurrentPlayers());
						m.teleportPlayer(e.getPlayer());
						MapManager.getInstance().setCurrentMap(e.getPlayer(), m);
					} else {
						Messages.form(e.getPlayer(),"Die Map ist §cvoll§7!");
					}

				}
			}
		}

	}

}
