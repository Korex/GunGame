package net.korex.gungame.maps;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import net.korex.gungame.maps.signs.MapSign;

public class Map {
	private String name;

	@Override
	public String toString() {
		return "Map{" +
				"name='" + name + '\'' +
				", currentPlayers=" + currentPlayers +
				", maxPlayers=" + maxPlayers +
				", sign=" + sign +
				", spawnPoint=" + spawnPoint +
				", spawnRegionMin=" + spawnRegionMin +
				", spawnRegionMax=" + spawnRegionMax +
				'}';
	}

	private int currentPlayers;
	private int maxPlayers;
	private Location sign;
	private Location spawnPoint;
	private Location spawnRegionMin, spawnRegionMax;

	public Map(String name, int maxPlayers, Location spawnPoint, Location spawnRegionMin, Location spawnRegionMax, Location sign) {
		this.name = name;
		this.maxPlayers = maxPlayers;
		this.spawnPoint = spawnPoint;
		this.spawnRegionMin = spawnRegionMin;
		this.spawnRegionMax = spawnRegionMax;
		this.sign = sign;
	}

	public void teleportPlayer(Player p) {
		p.teleport(this.spawnPoint);
	}

	public void removePlayerFromSign() {
		MapSign sign = new MapSign(this.sign);
		if (sign.getCurrentPlayersOnSign() > 0) {
			sign.setPlayer(sign.getCurrentPlayersOnSign() - 1);
		}
	}
	
	public Location getSpawnPoint() {
		return this.spawnPoint;
	}

	public String getName() {
		return this.name;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public Location getSign() {
		return sign;
	}

	public Location getSpawnRegionMax() {
		return spawnRegionMax;
	}

	public Location getSpawnRegionMin() {
		return spawnRegionMin;
	}

	public void setSpawnRegion(Location loc1, Location loc2) {
		int minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
		int minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
		int minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());

		int maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
		int maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
		int maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());

		this.spawnRegionMin = new Location(loc1.getWorld(), minX, minY, minZ);
		this.spawnRegionMax = new Location(loc1.getWorld(), maxX, maxY, maxZ);
	}

	public void setSign(Location sign) {
		this.sign = sign;
	}

	public void saveToConfig() {
		MapManager.getInstance().save(this);
	}

	public int getCurrentPlayers() {
		return currentPlayers;
	}

	public void setCurrentPlayers(int currentPlayers) {
		this.currentPlayers = currentPlayers;
	}
}
