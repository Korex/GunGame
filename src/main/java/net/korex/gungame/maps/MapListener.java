package net.korex.gungame.maps;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class MapListener implements Listener {
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if (MapManager.getInstance().inMap(e.getPlayer())) {
			MapManager.getInstance().removeFromCurrentMap(e.getPlayer());
			e.getPlayer().setHealth(0);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRespawn(PlayerRespawnEvent e) {
		if (MapManager.getInstance().inMap(e.getPlayer())) {
			Map m = MapManager.getInstance().getCurrentMap(e.getPlayer());
			e.setRespawnLocation(m.getSpawnPoint());
		}

	}

}
