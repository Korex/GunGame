package net.korex.gungame.maps;

import net.korex.gungame.FileManager;
import net.korex.gungame.LocationConverter;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class MapManager extends FileManager {

	private static MapManager instance = new MapManager();

	public static MapManager getInstance() {
		return instance;
	}

	private List<Map> maps;
	private HashMap<Player, Map> playerData;

	private MapManager() {
		super("maps");

		this.maps = new ArrayList<>();
		this.playerData = new HashMap<>();

		Set<String> set = this.getCfg().getKeys(false);
		for (String name : set) {
			int maxPlayers = this.getCfg().getInt(name + ".maxPlayers");
			Location spawnPoint = LocationConverter.convertFromString(this.getCfg().getString(name + ".spawnPoint"));

			Location signLoc = null;
			if(this.getCfg().contains(name + ".signLoc")) {
				String s = super.getCfg().getString(name + ".signLoc");
				signLoc = LocationConverter.convertFromString(s);
			}

			Location spawnRegionMin = null;
			if (this.getCfg().contains(name + ".spawnProtection.min")) {
				spawnRegionMin = LocationConverter.convertFromString(this.getCfg().getString(name + ".spawnProtection.min"));
			}

			Location spawnRegionMax = null;
			if (this.getCfg().contains(name + ".spawnProtection.max")) {
				spawnRegionMax = LocationConverter.convertFromString(this.getCfg().getString(name + ".spawnProtection.max"));
			}



			Map map = new Map(name, maxPlayers, spawnPoint, spawnRegionMin, spawnRegionMax, signLoc);
			System.out.println(map.toString());
			this.maps.add(map);
		}
	}

	public Map getMap(String mapName) {
		for (Map map : this.maps) {
			if(map.getName().equalsIgnoreCase(mapName)) {
				return map;
			}
		}
		return null;
	}

	public void remove(Map map) {
		this.getCfg().set(map.getName(), null);
		this.save();
		this.maps.remove(map);
	}

	public void save(Map map) {
		if(this.getMap(map.getName()) == null) {
			this.maps.add(map);
		}

		super.getCfg().set(map.getName() + ".maxPlayers", map.getMaxPlayers());
		super.getCfg().set(map.getName() + ".spawnPoint", LocationConverter.convertToString(map.getSpawnPoint()));

		if(map.getSign() != null) {
			this.getCfg().set(map.getName() + ".signLoc", LocationConverter.convertToString(map.getSign()));
		}

		if(map.getSpawnRegionMax() != null) {
			this.getCfg().set(map.getName() + ".spawnProtection.max", LocationConverter.convertToString(map.getSpawnRegionMax()));
		}

		if(map.getSpawnRegionMin() != null) {
			this.getCfg().set(map.getName() + ".spawnProtection.min", LocationConverter.convertToString(map.getSpawnRegionMin()));
		}

		super.save();
	}

	public void setCurrentMap(Player player, Map map) {
		playerData.put(player, map);
	}

	public void removeFromCurrentMap(Player player) {
		if (inMap(player)) {
			Map m = this.getCurrentMap(player);
			if(m.getCurrentPlayers() > 0) {
			    m.setCurrentPlayers(m.getCurrentPlayers() - 1);
            }
			m.removePlayerFromSign();
			playerData.remove(player);
		}
	}

	public Map getCurrentMap(Player player) {
		if (inMap(player)) {
			return playerData.get(player);
		}
		return null;
	}

	public boolean inMap(Player player) {
		return playerData.containsKey(player);
	}

	public boolean isPlayerInProtection(Player p) {
		if(!inMap(p)) {
			return false;
		}

		//iterating through all in case that player is in a connected map
		for (Map map : this.maps) {
			if(map.getSpawnRegionMin() == null || map.getSpawnRegionMax() == null) {
				continue;
			}

			Location min = map.getSpawnRegionMin();
			Location max = map.getSpawnRegionMax();

			if (p.getLocation().getX() >= min.getX() && p.getLocation().getX() <= max.getX()) {
				if (p.getLocation().getY() >= min.getY() && p.getLocation().getY() <= max.getY()) {
					if (p.getLocation().getZ() >= min.getZ() && p.getLocation().getZ() <= max.getZ()) {
						return true;
					}
				}

			}

		}

		return false;
	}

	public List<Map> getMaps() {
		return maps;
	}
}
