package net.korex.gungame.extraxp;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ExtraXpListener implements Listener{
	
	@EventHandler
	public void onInteractWithBottle(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getPlayer().getItemInHand().getType() == Material.EXP_BOTTLE) {
				e.setCancelled(true);
				e.getPlayer().setLevel(e.getPlayer().getLevel()+1);
				if(e.getPlayer().getItemInHand().getAmount() > 1) {
					e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount()-1);
				} else {
					e.getPlayer().getInventory().remove(e.getPlayer().getItemInHand());
				}
				e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.LEVEL_UP, 20, 1);
			}
		}
	}

}
