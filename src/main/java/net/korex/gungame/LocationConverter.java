package net.korex.gungame;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationConverter {
	public static Location convertFromString(String loc) {
		if (loc != null && !loc.isEmpty()) {
			if (loc.contains(",")) {
				String[] s = loc.split(",");

				String worldName = s[0];
				double x = Double.parseDouble(s[1]);
				double y = Double.parseDouble(s[2]);
				double z = Double.parseDouble(s[3]);
				float yaw = Float.parseFloat(s[4]);
				float pitch = Float.parseFloat(s[5]);

				return new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	public static String convertToString(Location loc) {
		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();
		float yaw = loc.getYaw();
		float pitch = loc.getPitch();
		String worldName = loc.getWorld().getName();

		return worldName + "," + x + "," + y + "," + z + "," + yaw + "," + pitch;
	}
}
