package net.korex.gungame.chatclear;

import net.korex.gungame.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.korex.gungame.Main;

public class ChatClear implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.hasPermission(Main.TEAM)) {
				for (Player players : Bukkit.getOnlinePlayers()) {
					for (int i = 0; i < 100; i++) {
						players.sendMessage("§c");
					}
					Messages.form(players, "Der Chat wurde von §a" + p.getName() + " §7gecleart!");
				}
			}

		}
		return false;
	}

}
