package net.korex.gungame.vote;

import net.korex.gungame.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VoteCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player) {
            Player p = (Player) sender;

            Messages.form(p, "Vote für GunGame und erhalte §a§lpro Vote ein Special Kit§r§7!");
            Messages.form(p, "Minecraft-Server.EU -> §3https://minecraft-server.eu/server/index/207FB/GunGame-Einzigartiger-Spielmodi-18-113");
            Messages.form(p, "Minecraft-Serverlist.net -> §3https://www.minecraft-serverlist.net/server/50842");
            Messages.form(p, "Serverliste.net -> §3http://www.serverliste.net/server/2391");
        }

        return true;
    }
}
