package net.korex.gungame.vote;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import net.korex.gungame.Main;
import net.korex.gungame.kits.kit.Kit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class VoteListener implements Listener {

    @EventHandler
    public void onVote(VotifierEvent e) {
        Vote vote = e.getVote();
        Bukkit.broadcastMessage(Main.PR + " §a§l" + vote.getUsername() + " §r§7hat auf §a§l" + vote.getServiceName() + " §r§7gevotet und als Belohnung das Vote Kit erhalten. Vote auch -> §a§l/vote");

        if(vote.getUsername() == null) {
            return;
        }

        Player p = Bukkit.getPlayer(vote.getUsername());

        if(p == null) {
            return;
        }

        for (ItemStack item : Kit.getKitById("VOTE").getItems()) {
            p.getInventory().addItem(item);
        }
    }

}
