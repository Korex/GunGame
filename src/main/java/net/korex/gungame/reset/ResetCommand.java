package net.korex.gungame.reset;

import net.korex.gungame.Messages;
import org.bukkit.entity.Player;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;
import net.korex.gungame.maps.Map;
import net.korex.gungame.maps.MapManager;
import net.korex.gungame.maps.signs.MapSign;

public class ResetCommand extends AbstractCommand {

	public ResetCommand() {
		super("resetSigns");
	}

	@Override
	public void onExecute(Player p, String[] args) {

		for (Map map : MapManager.getInstance().getMaps()) {
			if(map.getSign() == null) {
				continue;
			}
			map.setCurrentPlayers(0);
			MapSign sign = new MapSign(map.getSign());
			sign.setPlayer(0);
		}

		Messages.form(p, "Du hast alle Schilder §aresettet§7!");
	}

	@Override
	public void sendHelp(Player p) {
		p.sendMessage(Main.PR + "§a/gg resetSigns");
	}

}
