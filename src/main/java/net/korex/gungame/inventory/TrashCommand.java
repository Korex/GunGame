package net.korex.gungame.inventory;

import net.korex.gungame.Messages;
import net.korex.gungame.maps.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

public class TrashCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {

        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(MapManager.getInstance().inMap(p)) {
                Messages.form(p, "Du musst dich am Spawn befinden! §a/lobby§7!");
                return true;
            }

            Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
            p.openInventory(inv);

        }

        return false;
    }
}
