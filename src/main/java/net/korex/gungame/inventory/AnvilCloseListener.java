package net.korex.gungame.inventory;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;

public class AnvilCloseListener implements Listener {

    private HashMap<Player, Block> savedBlocks = new HashMap<>();

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.ANVIL) {
                savedBlocks.put(e.getPlayer(), e.getClickedBlock());
            }
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent e) {
        if (e.getInventory().getType() == InventoryType.ANVIL) {
            Player p = (Player) e.getPlayer();
            if (savedBlocks.containsKey(p)) {
                Block b = savedBlocks.get(p);
                b.setData((byte) 1);
                savedBlocks.remove(p);
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.savedBlocks.remove(e.getPlayer());
    }

}
