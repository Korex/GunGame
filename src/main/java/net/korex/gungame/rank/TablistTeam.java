package net.korex.gungame.rank;

import net.korex.gungame.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;

public class TablistTeam {

    private static HashMap<String, String> teams;

    static {
        teams = new HashMap<>();
    }


    public static void registerStandardTeams() {
        TablistTeam tablistTeam = new TablistTeam();

        tablistTeam.create("Team", 0, "§3Team§f ", Main.ADMIN);
        tablistTeam.create("Team", 1, "§3Team§f ", Main.TEAM);
        tablistTeam.create("Supreme", 2, "§6Supreme§f ", Main.SUPREMIUM);
        tablistTeam.create("Premium", 3, "§ePremium§f ", Main.PREMIUM);
        tablistTeam.create("Normal", 4, "§f", null);

        tablistTeam.update();
    }

    public void create(String name, int rank, String prefix, String permission) {
        String fullName = rank + "_" + name;
        Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
        Team t = board.getTeam(fullName);

        if (t != null) {
            t.unregister();
        }

        t = board.registerNewTeam(fullName);

        if (prefix != null) {
            t.setPrefix(prefix);
        }

        teams.put(permission, fullName);
    }

    public static void addAll() {
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            new TablistTeam().addPlayer(onlinePlayer);
        }
    }

    public void addPlayer(Player p) {
        Team t = null;
        for (String perm : teams.keySet()) {
            if (perm == null || p.hasPermission(perm)) {
                String currentTeamName = teams.get(perm);

                if (t == null || this.getRank(currentTeamName) < this.getRank(t.getName())) {
                    t = Bukkit.getScoreboardManager().getMainScoreboard().getTeam(currentTeamName);
                }
            }
        }

        if (t != null) {
            t.addPlayer(p);
        }
    }

    public void removePlayer(Player p) {
        for (String teamName : teams.values()) {
            Team t = Bukkit.getScoreboardManager().getMainScoreboard().getTeam(teamName);
            if (t != null && t.hasPlayer(p)) {
                t.removePlayer(p);
            }
        }
    }

    public void update() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.removePlayer(player);
            this.addPlayer(player);
        }
    }

    private int getRank(String teamName) {
        if (!teamName.contains("_") || teamName.split("_").length < 2) {
            return -1;
        }

        String stringI = teamName.split("_")[0];
        try {
            int i = Integer.parseInt(stringI);
            return i;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

}
