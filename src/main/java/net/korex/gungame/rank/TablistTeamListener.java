package net.korex.gungame.rank;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class TablistTeamListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        new TablistTeam().addPlayer(e.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        new TablistTeam().removePlayer(e.getPlayer());
    }
}
