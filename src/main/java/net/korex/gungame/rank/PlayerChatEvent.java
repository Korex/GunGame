package net.korex.gungame.rank;

import net.korex.gungame.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChatEvent implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if(e.getPlayer().hasPermission(Main.TEAM)) {
            String message = e.getMessage();
            if(e.getMessage().contains("&")) {
                message = e.getMessage().replace("&","§");
            }
            e.setFormat("§f[§3Team§f] §3" + e.getPlayer().getName() + "§f: " + message);
        } else if(e.getPlayer().hasPermission(Main.SUPREMIUM)) {
            e.setFormat("§7[§6Supreme§7] §6" + e.getPlayer().getName() + "§7: " + e.getMessage());
        } else if(e.getPlayer().hasPermission(Main.PREMIUM)) {
            e.setFormat("§7[§ePremium§7] §e" + e.getPlayer().getName() + "§7: " + e.getMessage());
        } else {
            e.setFormat("§f" + e.getPlayer().getName() + ": §7" + e.getMessage());
        }
    }
}
