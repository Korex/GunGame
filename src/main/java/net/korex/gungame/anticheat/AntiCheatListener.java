package net.korex.gungame.anticheat;

import net.korex.gungame.Messages;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class AntiCheatListener implements Listener{

	@EventHandler
	public void onInvenntoryOpen(InventoryOpenEvent e) {
		Player p = (Player) e.getPlayer();
		if(AntiCheatInventory.containsPlayer(p)) {
			e.setCancelled(true);
			Messages.form(p, "Da du im GameMode 1 bist, kannst du §ckeine Inventare §7öffnen!");
		}
	}
	
	@EventHandler
	public void onLogout(PlayerQuitEvent e) {
		if(AntiCheatInventory.containsPlayer(e.getPlayer())) {
			new AntiCheatInventory(e.getPlayer()).setOldInventory();
			e.getPlayer().setGameMode(GameMode.SURVIVAL);
		}
	}

}
