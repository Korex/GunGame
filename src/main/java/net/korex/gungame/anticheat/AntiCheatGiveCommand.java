package net.korex.gungame.anticheat;

import net.korex.gungame.Messages;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.korex.gungame.Main;

public class AntiCheatGiveCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {

			Player p = (Player) sender;

			if (p.hasPermission(Main.ADMIN)) {
				if (args.length == 1) {
					if (args[0].equalsIgnoreCase("barrier")) {
						p.getInventory().addItem(new ItemStack(Material.BARRIER, 1));
					} else if (args[0].equalsIgnoreCase("commandblock")) {
						p.getInventory().addItem(new ItemStack(Material.COMMAND, 1));
					} else {
						Messages.form(p, "§a/give <barrier/commandblock>");
					}
				} else {
					Messages.form(p, "§a/give <barrier/commandblock>");
				}
			}

		}
		return true;
	}

}
