package net.korex.gungame.anticheat;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class AntiCheatInventory {

	private static HashMap<Player, ItemStack[]> inventory;
	private static HashMap<Player, ItemStack[]> inventoryArmor;
	private static HashMap<Player, Integer> level;

	private Player p;

	static {
		inventory = new HashMap<>();
		inventoryArmor = new HashMap<>();
		level = new HashMap<>();
	}

	public AntiCheatInventory(Player p) {
		this.p = p;
	}

	public static boolean containsPlayer(Player p) {

		if (inventory.containsKey(p) && level.containsKey(p) && inventoryArmor.containsKey(p)) {
			return true;
		}

		return false;
	}

	public void removeOldInventory() {
		if (!inventory.containsKey(this.p) && !level.containsKey(this.p) && !inventoryArmor.containsKey(this.p)) {
			inventory.put(this.p, this.p.getInventory().getContents());

			inventoryArmor.put(this.p, this.p.getInventory().getArmorContents());

			level.put(this.p, this.p.getLevel());

			for (int i = 0; i < 40; i++) {
				this.p.getInventory().setItem(i, new ItemStack(Material.AIR));
			}

		}

	}

	public void setOldInventory() {
		if (inventory.containsKey(this.p) && level.containsKey(this.p)) {

			for (int i = 0; i < 40; i++) {
				this.p.getInventory().setItem(i, new ItemStack(Material.AIR));
			}

			this.p.getInventory().setContents(inventory.get(this.p));
			inventory.remove(this.p);
			
			this.p.getInventory().setArmorContents(inventoryArmor.get(this.p));
			inventoryArmor.remove(this.p);

			this.p.setLevel(level.get(this.p));
			level.remove(this.p);
		}

	}
}
