package net.korex.gungame.anticheat;

import net.korex.gungame.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.korex.gungame.Main;

public class AntiCheatXPCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.hasPermission(Main.ADMIN)) {
				Messages.form(p, "Das XP-Geben ist hier §cverboten§7!");
			}

		}

		return true;
	}

}
