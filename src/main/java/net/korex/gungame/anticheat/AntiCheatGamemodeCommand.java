package net.korex.gungame.anticheat;

import net.korex.gungame.Messages;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.korex.gungame.Main;

public class AntiCheatGamemodeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (p.hasPermission(Main.ADMIN)) {
                if (args.length == 1 || args.length == 2) {
                    try {
                        int i = Integer.parseInt(args[0]);

                        Player target;
                        if(args.length == 2) {
                            target = Bukkit.getPlayer(args[1]);
                            if(target == null) {
                                Messages.form(p, target.getName() + " ist §cnicht online§7!");
                            }
                        } else {
                            target = p;
                        }

                        AntiCheatInventory inv = new AntiCheatInventory(target);

                        if (i == 0) {
                            target.setGameMode(GameMode.SURVIVAL);
                            inv.setOldInventory();
                        } else if (i == 1) {
                            target.setGameMode(GameMode.CREATIVE);
                            inv.removeOldInventory();
                        } else if (i == 2) {
                            target.setGameMode(GameMode.ADVENTURE);
                            inv.setOldInventory();
                        } else if (i == 3) {
                            target.setGameMode(GameMode.SPECTATOR);
                            inv.removeOldInventory();
                        }
                        Messages.form(p, target.getName() + " ist jetzt im §aGamemode " + i + "§7!");
                    } catch (NumberFormatException e) {
                        Messages.form(p, "§a/gamemode <1-3>");
                    }
                } else {
                    Messages.form(p, "§a/gamemode <1-3>");
                }
            }

        }

        return true;
    }

}
