package net.korex.gungame;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GunGameCommand implements CommandExecutor {

	// /gg map add <Name> <maxPlayers>
	// /gg map remove <Name>
	// /gg setKit <lvl>
	// /gg removeKit <lvl>
	// /gg tpall

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {

			Player p = (Player) sender;

			if (p.hasPermission(Main.ADMIN)) {
				if (args.length > 0) {
					String s = args[0];

					if (AbstractCommand.containsCommand(s.toLowerCase())) {
						AbstractCommand.getCommand(s.toLowerCase()).onExecute(p, args);
					} else {
						sendHelp(p);
					}
				} else {
					sendHelp(p);
				}
			}

		}
		return true;
	}

	private void sendHelp(Player p) {
		for (AbstractCommand a : AbstractCommand.getSubCommands().values()) {
			a.sendHelp(p);
		}
	}

	// if (args[0].equalsIgnoreCase("map")) {
	// if (args.length == 2) {
	// if (args[1].equalsIgnoreCase("add")) {
	// String mapName = args[2];
	// int maxPlayers = Integer.parseInt(args[3]);
	// Location spawnLocation = p.getLocation();
	// Map m = new Map(mapName, maxPlayers, spawnLocation);
	// m.add();
	// } else {
	//
	// }
	// } else if (args.length == 3) {
	// if (args[1].equalsIgnoreCase("remove")) {
	// String mapName = args[2];
	// Map m = MapManager.getMap(mapName);
	// if (m != null) {
	// m.remove();
	// }
	// } else {
	//
	// }
	// }
	//
	// }

}
