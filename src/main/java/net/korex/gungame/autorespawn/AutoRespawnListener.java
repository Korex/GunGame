package net.korex.gungame.autorespawn;

import net.korex.gungame.Main;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class AutoRespawnListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
            e.getEntity().spigot().respawn();
        }, 10);
    }
}
