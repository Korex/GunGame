package net.korex.gungame;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileManager {
	
	private File file;
	private YamlConfiguration cfg;
	private String fileName;

	public FileManager(String fileName) {
		this.fileName = fileName;
		this.reload();
	}

	public void reload() {
		this.file = new File(Main.getInstance().getDataFolder(), fileName.toLowerCase() + ".yml");
		this.cfg = YamlConfiguration.loadConfiguration(this.file);
	}
	
	public YamlConfiguration getCfg() {
		return this.cfg;
	}
	
	public void save() {
		try {
			this.cfg.save(this.file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
