package net.korex.gungame.disallowedlisteners;

import net.korex.gungame.Main;
import net.korex.gungame.kits.level.Level;
import net.korex.gungame.lobby.LobbyLocation;
import net.korex.gungame.maps.MapManager;
import org.bukkit.GameMode;
import org.bukkit.Rotation;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class DisallowedListeners implements Listener {

    private boolean check(Player p) {
        if (p.hasPermission(Main.ADMIN) && p.getGameMode() == GameMode.CREATIVE) {
            return true;
        }
        return false;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e) {
        System.out.println(new LobbyLocation().getLocation());
        e.getPlayer().teleport(new LobbyLocation().getLocation());
        e.setJoinMessage("§7[§a+§7] §a" + e.getPlayer().getDisplayName());
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!check(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDead(PlayerDeathEvent e) {
        e.setDeathMessage("");
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage("§7[§c-§7] §c" + e.getPlayer().getDisplayName());
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            if (e.getCause() == DamageCause.FALL) {
                e.setCancelled(true);
                return;
            }
            Player p = (Player) e.getEntity();
            if(!MapManager.getInstance().inMap(p)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!check(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onRespawn(PlayerRespawnEvent e) {
        if(!MapManager.getInstance().inMap(e.getPlayer())) {
            e.setRespawnLocation(new LobbyLocation().getLocation());
        }

        Level k = new Level();
        k.setKit(e.getPlayer(), 0);
    }

    @EventHandler
    public void onInteractAtFrames(PlayerInteractAtEntityEvent e) {
        if (e.getRightClicked().getType() == EntityType.ITEM_FRAME) {
            if (!check(e.getPlayer())) {
                ItemFrame frame = (ItemFrame) e.getRightClicked();
                frame.setRotation(Rotation.COUNTER_CLOCKWISE);
                e.setCancelled(true);
            }

        }

    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        e.setDroppedExp(0);
        e.getDrops().clear();
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onThunderChange(ThunderChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDmg(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            Player p = (Player) e.getDamager();
            if (e.getEntityType() == EntityType.ITEM_FRAME) {
                if (!check(p)) {
                    e.setCancelled(true);
                }
            }

        }

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if(e.getMessage().contains("%")) {
            e.setMessage(e.getMessage().replace("%", "Prozent"));
        }
    }

    @EventHandler
    public void onDisconnectSpam(PlayerKickEvent e) {
        if(e.getReason().equals("disconnect.spam")) {
            e.setCancelled(true);
        }
    }

}
