package net.korex.gungame.shop;

import org.bukkit.Location;
import org.bukkit.block.Sign;

public class ShopSign {

    private Sign s;

    private static String prefix;

    public ShopSign(Location signLoc) {

        if (signLoc.getBlock().getState() instanceof Sign) {
            Sign locSign = (Sign) signLoc.getBlock().getState();

            if (locSign.getLine(0).equals(prefix)) {
                s = locSign;
            }
        }

    }

    static {
        prefix = "§7[§bShop§7]";
    }

    public static String getPrefix() {
        return ShopSign.prefix;
    }

    public int getID() {
        String sID = s.getLine(1);
        if (sID.contains(":")) {
            sID = sID.split(":")[0];
        }
        sID = sID.replace("§a", "");
        int id = Integer.parseInt(sID);
        return id;
    }

    public int getSubID() {
        if (s.getLine(1).contains(":")) {
            int id = Integer.parseInt(s.getLine(1).split(":")[1]);
            return id;
        } else {
            return 0;
        }
    }

    public int getPrice() {
        String sPrice = s.getLine(2);
        sPrice = sPrice.replace("§b", "");
        sPrice = sPrice.replace(" ", "");
        sPrice = sPrice.replace("GP", "");
        int price = Integer.parseInt(sPrice);
        return price;
    }

    public int getAmount() {
        String sAmount = s.getLine(3);
        sAmount = sAmount.replace("§7", "");
        sAmount = sAmount.replace(" ", "");
        sAmount = sAmount.replace("Piece", "");
        int price = Integer.parseInt(sAmount);
        return price;
    }

    public String formID(String id) {
        String s = "§a" + id;
        return s;
    }

    public String formPrice(String price) {
        return "§b" + price + " GP";
    }

    public String formPiece(String piece) {
        return "§7" + piece + " Piece";
    }

}
