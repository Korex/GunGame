package net.korex.gungame.shop;

import net.korex.gungame.Messages;
import net.korex.gungame.stats.Stats;
import net.korex.gungame.stats.StatsManager;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class RepairSignListener implements Listener {

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        if (e.getLine(0).equals("GunGame-Repair")) {
            RepairSign sign = new RepairSign(e.getBlock().getLocation());

            e.setLine(0, RepairSign.getPrefix());
            e.setLine(1, sign.formItemName(e.getLine(1)));
            e.setLine(2, sign.formPrice(e.getLine(2)));
        }
    }

    @EventHandler
    public void onInteractAtSign(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getState() instanceof Sign) {
                Sign s = (Sign) e.getClickedBlock().getState();

                if (s.getLine(0).equals(RepairSign.getPrefix())) {
                    RepairSign sign = new RepairSign(e.getClickedBlock().getLocation());
                    int price = sign.getPrice();

                    Stats stats = StatsManager.getInstance().getFromCache(e.getPlayer());

                    if(stats == null) {
                        return;
                    }

                    if (price <= stats.getCoins()) {
                        String itemName = sign.getItemName();

                        if(e.getPlayer().getItemInHand().getType().name().toLowerCase().startsWith(itemName.toLowerCase())) {
                            stats.setCoins(stats.getCoins() - price);
                            ItemStack item = e.getPlayer().getItemInHand();
                            item.setDurability((short) 0);
                            e.getPlayer().getInventory().remove(e.getPlayer().getItemInHand());
                            e.getPlayer().getInventory().setItemInHand(item);
                            Messages.form(e.getPlayer(), "Du hast das Item §arepariert§7!");
                        } else {
                            Messages.form(e.getPlayer(), "Du kannst hier nur Items des Typs §c" + itemName + "§7reparieren!");
                            return;
                        }
                    } else {
                        Messages.form(e.getPlayer(), "Du hast §cnicht genug§7 Coins!");
                    }
                }
            }
        }
    }

}
