package net.korex.gungame.shop;

import net.korex.gungame.Messages;
import net.korex.gungame.stats.StatsManager;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import net.korex.gungame.stats.Stats;

public class ShopSignListener implements Listener {

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        if (e.getLine(0).equals("GunGame-Shop")) {
            ShopSign sign = new ShopSign(e.getBlock().getLocation());

            e.setLine(0, ShopSign.getPrefix());
            e.setLine(1, sign.formID(e.getLine(1)));
            e.setLine(2, sign.formPrice(e.getLine(2)));
            e.setLine(3, sign.formPiece(e.getLine(3)));
        }
    }

    @EventHandler
    public void onInteractAtSign(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getState() instanceof Sign) {
                Sign s = (Sign) e.getClickedBlock().getState();

                if (s.getLine(0).equals(ShopSign.getPrefix())) {
                    ShopSign sign = new ShopSign(e.getClickedBlock().getLocation());
                    int price = sign.getPrice();

                    Stats stats = StatsManager.getInstance().getFromCache(e.getPlayer());

                    if (stats == null) {
                        return;
                    }

                    if (price <= stats.getCoins()) {
                        int id = sign.getID();
                        int subId = sign.getSubID();
                        int amount = sign.getAmount();

                        ItemStack item;
                        if (subId == 0) {
                            item = new ItemStack(id, amount);
                        } else {
                            item = new ItemStack(id, amount, (short) 0, (byte) subId);
                        }

                        if (e.getPlayer().getInventory().firstEmpty() == -1) {
                            Messages.form(e.getPlayer(), "Dein Inventar ist §cvoll§7!");
                            return;
                        }

                        e.getPlayer().getInventory().addItem(item);

                        stats.setCoins(stats.getCoins() - price);

                    } else {
                        Messages.form(e.getPlayer(), "Du hast §cnicht genug§7 Coins!");
                    }
                }
            }
        }
    }

}
