package net.korex.gungame.shop;

import org.bukkit.Location;
import org.bukkit.block.Sign;

public class RepairSign {

    private Sign s;

    private static String prefix;

    public RepairSign(Location signLoc) {

        if (signLoc.getBlock().getState() instanceof Sign) {
            Sign locSign = (Sign) signLoc.getBlock().getState();

            if (locSign.getLine(0).equals(prefix)) {
                s = locSign;
            }
        }

    }

    static {
        prefix = "§7[§bRepair§7]";
    }

    public static String getPrefix() {
        return RepairSign.prefix;
    }

    public String getItemName() {
        String itemName = s.getLine(1);
        itemName = itemName.replace("§a", "");
        return itemName;
    }

    public int getPrice() {
        String sPrice = s.getLine(2);
        sPrice = sPrice.replace("§b", "");
        sPrice = sPrice.replace(" ", "");
        sPrice = sPrice.replace("GP", "");
        int price = Integer.parseInt(sPrice);
        return price;
    }

    public String formPrice(String price) {
        return "§b" + price + " GP";
    }

    public String formItemName(String itemName) {
        String s = "§a" + itemName;
        return s;
    }

}
