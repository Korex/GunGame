package net.korex.gungame.regeneration;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class RegenerationListener implements Listener{
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		if(e.getEntity().getKiller() instanceof Player) {
			Player p  = e.getEntity().getKiller();
			if(p.getLevel() < 20) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 40, 4, false, false));
			} 
			
		}
	}

}
