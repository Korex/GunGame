package net.korex.gungame.vanish;

import net.korex.gungame.Main;
import net.korex.gungame.Messages;
import net.korex.gungame.lobby.LobbyLocation;
import net.korex.gungame.maps.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VanishCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;

            if (p.hasPermission(Main.TEAM)) {

                if (args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0]);

                    if (target != null) {
                        p.teleport(target);
                        p.setGameMode(GameMode.SPECTATOR);
                        Messages.form(p, "Du beobachtest nun §c" + target.getName() + "§7");
                    } else {
                        Messages.form(p, "Der angegebene Spieler ist §coffline§7!");
                    }
                } else if (args.length == 0) {
                    if (p.getGameMode() == GameMode.SPECTATOR) {
                        p.setGameMode(GameMode.SURVIVAL);

                        if(MapManager.getInstance().inMap(p)) {
                            MapManager.getInstance().removeFromCurrentMap(p);
                        }

                        p.teleport(new LobbyLocation().getLocation());
                    } else {
                        Messages.form(p, "§a/vanish <Name>");
                    }
                } else {
                    Messages.form(p, "§a/vanish <Name>");
                }

            }

        }
        return true;
    }

}
