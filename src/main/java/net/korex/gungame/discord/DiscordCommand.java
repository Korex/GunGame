package net.korex.gungame.discord;

import net.korex.gungame.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DiscordCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {

        if(sender instanceof Player) {
            Player p = (Player) sender;

            Messages.form(p, "Trete unserem Discord Server bei, um Kontakt mit Spielern oder mit dem Team zu haben. Unser Discord Server: §a§lhttps://discord.gg/sXHGrJ5 ");
        }

        return true;
    }
}
