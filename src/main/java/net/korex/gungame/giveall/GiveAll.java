package net.korex.gungame.giveall;

import net.korex.gungame.Messages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.korex.gungame.AbstractCommand;
import net.korex.gungame.Main;

public class GiveAll extends AbstractCommand {

    public GiveAll() {
        super("giveall");

    }

    @Override
    public void onExecute(Player p, String[] args) {
        for (Player players : Bukkit.getOnlinePlayers()) {
            players.getInventory().addItem(p.getInventory().getItemInHand());
            Messages.form(players, "Du hast §a" + p.getInventory().getItemInHand().getType().toString() + " §7erhalten!");
        }
    }

    @Override
    public void sendHelp(Player p) {
        p.sendMessage(Main.PR + "§a/gg giveall");
    }

}
